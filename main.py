import requests
import configparser
import time
import hashlib
from dataclasses import dataclass


CONFIG_PATH = "./config.ini"
SECERT = "Eshore!@#"
ISWIFI = "4060"
SESSION = requests.session()
SESSION.headers[
    "User-Agent"
] = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.87 Safari/537.36"
NASIP = "119.146.175.80"


@dataclass
class Config:
    account: str
    password: str
    local_ip: str
    mac_address: str


def print_error_and_exit(data: str):
    print(f"ERROR: {data}")
    exit(-1)


def read_config(path: str) -> Config:
    config = configparser.ConfigParser()
    config.read(path)
    config_object = Config(
        config["record"]["account"],
        config["record"]["password"],
        config["record"]["local_ip"],
        config["record"]["mac_address"],
    )

    return config_object


def auth():
    response = SESSION.get(
        "http://172.17.18.2:8080/byod/templatePage/20200318110733542/guestRegister.jsf",
        timeout=3,
    )
    cookies = response.headers.get("Set-Cookie", "")
    if cookies == "":
        print_error_and_exit("没有获取到认证cookie！")
    print("获取认证cookie成功，正在激活cookie...")
    response = SESSION.get(
        "http://172.17.18.2:8080/byod/index.xhtml", timeout=3
    )
    print(response.text)
    auth_text = input('请输入 id="javax.faces.ViewState" value="XXX" 的内容：')
    response = SESSION.post(
        "http://172.17.18.2:8080/byod/index.xhtml",
        allow_redirects=True,
        timeout=3,
    )
    print(response.headers)


def get_md5_str(s: str) -> str:
    return hashlib.md5(s.encode("utf-8")).hexdigest().upper()


def get_verify_code(config: Config) -> str:
    url = "http://enet.10000.gd.cn:10001/client/challenge"
    time_stamp = str(time.time_ns())[:13]
    md5_string = get_md5_str(
        config.local_ip + NASIP + config.mac_address + time_stamp + SECERT
    )
    data = {
        "username": config.account,
        "clientip": config.local_ip,
        "nasip": NASIP,
        "mac": config.mac_address,
        "timestamp": time_stamp,
        "authenticator": md5_string,
    }
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.87 Safari/537.36",
        "Content-Type": "application/json",
        "Accept": "*/*",
        "Host": "enet.10000.gd.cn:10001",
    }
    response = SESSION.post(url, json=data, headers=headers, timeout=3)
    verify_code = response.json()["challenge"]
    return verify_code


def login(verify_code: str) -> str:
    url = "http://enet.10000.gd.cn:10001/client/login"
    time_stamp = str(time.time_ns())[:13]
    md5_string = get_md5_str(
        config.local_ip
        + NASIP
        + config.mac_address
        + time_stamp
        + verify_code
        + SECERT
    )
    data = {
        "username": config.account,
        "password": config.password,
        "clientip": config.local_ip,
        "nasip": NASIP,
        "mac": config.mac_address,
        "iswifi": ISWIFI,
        "timestamp": time_stamp,
        "authenticator": md5_string,
    }
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.87 Safari/537.36",
        "Content-Type": "application/json",
        "Accept": "*/*",
        "Host": "enet.10000.gd.cn:10001",
    }
    response = SESSION.post(url, json=data, headers=headers, timeout=3)

    return response.text


def keep():
    url = "http://enet.10000.gd.cn:8001/hbservice/client/active"
    time_stamp = str(time.time_ns())[:13]
    md5_string = get_md5_str(
        config.local_ip
        + NASIP
        + config.mac_address
        + time_stamp
        + SECERT
    )
    payload = {
        "username": config.account,
        "clientip": config.local_ip,
        "nasip": NASIP,
        "mac": config.mac_address,
        "timestamp": time_stamp,
        "authenticator": md5_string,
    }
    SESSION.post(url, params=payload, timeout=3)


if __name__ == "__main__":
    config = read_config(CONFIG_PATH)
    # try:
    #     status_code = SESSION.get(
    #         "http://172.17.18.3:8080/portal/", timeout=3
    #     ).status_code
    #     if status_code != 200 and status_code != 302:
    #         print_error_and_exit("当前没有在学校局域网内，请尝试断开wifi后重连")
    # except:
    #     print_error_and_exit("当前没有在学校局域网内，请尝试断开wifi后重连")

    # test_url = "http://www.qq.com"
    # response = SESSION.get(test_url, timeout=3)
    # if response.status_code == 200:
    #     print("当前为纯学校wifi，需要登录认证，正在自动认证中...")
    #     auth()
    # elif response.status_code == 302:
    #     print("当前网络已为畅通状态，无需再次登录")
    # else:
    #     print_error_and_exit("获取到了非200、302的状态码，登录失败！请检查网络是否正常" + response.text)

    verify_code = get_verify_code(config)
    print(f"获取验证码为：{verify_code}")
    login_response = login(verify_code)
    print(f"登录返回的信息为：{login_response}")
    if "success" in login_response:
        print("登录成功！接下来将维持连接")
    while True:
        time.sleep(20)
        keep()
